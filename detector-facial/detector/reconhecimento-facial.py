import os
import sys
import dlib
import cv2
import numpy as np

# Detector facial generico do dlib
detectorFaces = dlib.get_frontal_face_detector()

# Arquivo contendo as informações referentes aos 68 pontos faciais
# TensorFlow e Facenet
detectorPontos = dlib.shape_predictor("../recursos/shape_predictor_68_face_landmarks.dat")

# Responsavel por aplicar as 27 covoluções para escolher quais são as melhores características
reconhecimentoFacial = dlib.face_recognition_model_v1("../recursos/dlib_face_recognition_resnet_model_v1.dat")

# Carrega os indices do arquivo criado no imagens
indices = np.load("../arquivos/indices.pickle")
# Carrega os descritores faciais do arquivo criado no imagens
descritoresFaciais = np.load("../arquivos/descritores.npy")

# Valor limiar
# Recomendado 0.50
limiar = 0.50

captura = cv2.VideoCapture(1)

while captura.isOpened():
    conectado, frame = captura.read()

    facesDetectadas = detectorFaces(frame, 1)

    for face in facesDetectadas:
        # ~~Leitura e reconhecimento dos pontos faciais~~
        # Identifica os pontos faciais
        pontosFaciais = detectorPontos(frame, face)
        # Vai descrever uma face tendo como base os descritores da face detectada
        # Resultado: vetor com 128 posições que descreve a face encontrada
        # Em suma, é o "imagens"
        # Aplica as chamadas "funções de kernel"
        descritorFacial = reconhecimentoFacial.compute_face_descriptor(frame, pontosFaciais)

        # ~~Conversão dos dados~~
        # Convertendo os dados para os mesmos do imagens
        # Percorre o vetor descritorFacial
        listaDescritorFacial = [fd for fd in descritorFacial]
        # Converte em um npArray
        npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
        # Aumenta a dimensão do vetor, adicionando 1 coluna
        # Será usado para armazenar o descritor facial
        npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]

        # ~~Algorítmo KNN~~
        # Compara os valores adquirido na detecção pelos valores do treinado
        # Mede a distância euclidiana e normaliza os valores
        distancias = np.linalg.norm(npArrayDescritorFacial - descritoresFaciais, axis=1)
        # Verifica o indice com a menor distância euclidiana normalizada
        minimo = np.argmin(distancias)
        # Verifica o valor de acordo com a posição
        distanciaMinima = distancias[minimo]

        cor = (0, 0, 0)
        cor_texto = (0, 0, 0)

        # Compara o valor da distância com o limiar
        if distanciaMinima <= limiar:
            # Só para pegar o nome da "imagem" dentro do indice
            nome = os.path.split(indices[minimo])[1].split(".")[0]
            cor = (0, 255, 0)
        else:
            nome = "Desconhecido"
            cor = (0, 0, 255)
            cor_texto = (255, 255, 255)

        # ~~Exibição e Apresentação~~
        # Separa as coordenadas da face identificada
        e, t, d, b = (int(face.left()), int(face.top()), int(face.right()), int(face.bottom()))

        # Cria o retângulo na imagem com as coordenadas da face identificada
        cv2.rectangle(frame, (e, t), (d, b), cor, 2)

        # Cria um retângulo preenchido para colocar de fundo no texto
        cv2.rectangle(frame, (e - 1, t - 30), (d + 1, t), cor, cv2.FILLED)

        # Cria o texto e coloca o nome da face e o valor da distância
        if nome != "Desconhecido":
            tag = "{} {:.2f}%".format(nome, 100*(1 - distanciaMinima))
        else:
            tag = nome

        # Insere o texto de acordo com as coordenadas passada
        cv2.putText(frame, tag, (e, (t - 10)), cv2.FONT_HERSHEY_PLAIN, 1.3, cor_texto)

        # Exibe a janela com a captura da webcam
        cv2.imshow("Detector e Identificador Facial", frame)

    # Aguarda pressionar a tecla Esq para fechar a janela
    if cv2.waitKey(1) & 0xFF == 27:
        break

# Finaliza as janelas
captura.release()
cv2.destroyAllWindows()
sys.exit(0)
