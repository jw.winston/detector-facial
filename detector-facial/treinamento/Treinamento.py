import os
import glob
import _pickle as cPickle
import dlib
import cv2
import numpy as np

from twisted.internet import reactor, task


def Treinamento():

    print("Iniciando Treinamento.")
    print("============================================")

    # Detector facial generico do dlib
    detectorFaces = dlib.get_frontal_face_detector()

    # Arquivo contendo as informações referentes aos 68 pontos faciais
    # TensorFlow e Facenet
    detectorPontos = dlib.shape_predictor("../recursos/shape_predictor_68_face_landmarks.dat")

    # Responsavel por aplicar as 27 covoluções para escolher quais são as melhores características
    reconhecimentoFacial = dlib.face_recognition_model_v1("../recursos/dlib_face_recognition_resnet_model_v1.dat")

    # Controladores
    indice = {}
    idx = 0
    descritoresFaciais = None

    # ~~Controle de processo~~
    n = 1

    # Percorrer a pasta de imagens
    for arquivo in glob.glob(os.path.join("imagens", "*.jpg")):
        # ~~Controle de processo~~
        print("--------------------------------------------")
        print("Arquivo {}: {}".format(n, arquivo))

        # Abre a imagem
        imagem = cv2.imread(arquivo)
        # Aplica a detecção de face genérica
        faceDetectada = detectorFaces(imagem, 1)
        # Armazena a quantidade de faces detectadas na imagem
        numeroFacesDetectadas = len(faceDetectada)

        # Verifica a quantidade de faces detectadas
        # Deve haver apenas 1 (uma) face detectada para realizar o imagens
        # Evita erros
        if numeroFacesDetectadas > 1:
            print("Há mais de uma face na imagem {}".format(arquivo))
            continue
        elif numeroFacesDetectadas < 1:
            print("Nenhuma face encontrada no arquivo {}".format(arquivo))
            continue

        # Percorre os dados obtidos da detecção
        # Somente a área detectada
        for face in faceDetectada:
            # ~~Controle de processo~~
            print("Analise da área detectada em {}".format(face))

            # ~~Leitura e reconhecimento dos pontos faciais~~
            # Identifica os pontos faciais
            pontosFaciais = detectorPontos(imagem, face)

            # Vai descrever uma face tendo como base os descritores da face detectada
            # Resultado: vetor com 128 posições que descreve a face encontrada
            # Em suma, é o "imagens"
            # Aplica as chamadas "funções de kernel"
            descritorFacial = reconhecimentoFacial.compute_face_descriptor(imagem, pontosFaciais)

            # ~~Conversão dos dados~~
            # Percorre o vetor descritorFacial
            listaDescritorFacial = [df for df in descritorFacial]
            # Converte em um npArray
            npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
            # Aumenta a dimensão do vetor, adicionando 1 coluna
            # Será usado para armazenar o descritor facial
            npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]

            # ~~Concatenar os dados~~
            # Cada uma das linhas terá os 128 valores referente ao descritor facial
            if descritoresFaciais is None:
                descritoresFaciais = npArrayDescritorFacial
            else:
                descritoresFaciais = np.concatenate((descritoresFaciais, npArrayDescritorFacial), axis=0)

            # ~~Relacionar descritores ao nome das imagens~~
            # Pois este arquivo possui mais de uma face
            indice[idx] = arquivo
            idx += 1

            # ~~Controle de processo~~
            print("Processo {} finalizado".format(n))
            n += 1

    # Conjunto de 128 informações relevantes referentes às faces
    np.save("../arquivos/descritores.npy", descritoresFaciais)

    # Listagem de cada uma das imagens
    with open("../arquivos/indices.pickle", 'wb') as f:
        cPickle.dump(indice, f)

    # ~~Controle de processo~~
    print("============================================")
    print("Treinamento concluido.")

    print("\n----------------------------------------------------------------------")
    print("----------------------------------------------------------------------\n")


if __name__ == '__main__':
    t = 30.0

    l = task.LoopingCall(Treinamento)
    l.start(t)
    reactor.run()
